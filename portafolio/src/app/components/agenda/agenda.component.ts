import { Component, OnInit, ViewChild } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DialogoComponent } from '../dialogo/dialogo.component';
import { ApiService } from '../services/api.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.css']
})
export class AgendaComponent implements OnInit {

  displayedColumns: string[] = ['Nombre', 'Apellidos', 'Email', 'Celular', 'Hora', 'Servicio', 'Fecha',  'Descripcion'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor( private dialog : MatDialog, private api : ApiService) { }

  ngOnInit(): void {
    this.getAllClientes();
  }

  openDialog() {
    this.dialog.open(DialogoComponent, {
     width: '60%', 
    });
  }
  getAllClientes() {
    this.api.getClientes()
    .subscribe({
      next: (res) => {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort  
      },
      error: (err) => {
        alert("Error de busqueda!")
      }
    })
  }
  

}
