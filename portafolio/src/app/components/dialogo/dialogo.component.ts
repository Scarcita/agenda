import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, FormBuilder, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { ApiService } from '../services/api.service';
import { MatDialogRef } from '@angular/material/dialog';

export class MyErrorStateMatcher implements ErrorStateMatcher {

  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): 
  boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-dialogo',
  templateUrl: './dialogo.component.html',
  styleUrls: ['./dialogo.component.css']
})
export class DialogoComponent implements OnInit{

  

  emailFormControl = new FormControl('', [Validators.required, Validators.email]);

  matcher = new MyErrorStateMatcher();

  productForm!: FormGroup

  constructor(private formBuilder : FormBuilder,
              private api: ApiService,
              private dialogRef : MatDialogRef<DialogoComponent>) { }

  ngOnInit(): void {
    this.productForm = this.formBuilder.group({
      Nombre : ['', Validators.required],
      Apellidos : ['', Validators.required],
      Email : ['', Validators.required],
      Celular : ['', Validators.required],
      Hora : ['', Validators.required],
      Servicio : ['', Validators.required],
      Fecha : ['', Validators.required],
      Descripcion : ['', Validators.required],
    })
  }

  enviarCita() {
    if(this.productForm.valid){
      this.api.postClientes(this.productForm.value)
      .subscribe({
        next: (res)=> {
          alert("Cita añadida a la agenda");
          this.productForm.reset();
          this.dialogRef.close("Guardado");
        },
        error: () => {
          alert("Error al agendar")
        }
      })
    }
    applyFilter(event: Event) {
      const filterValue = (event.target as HTMLInputElement).value;
      this.dataSource.filter = filterValue.trim().toLowerCase();
  
      if (this.dataSource.paginator) {
        this.dataSource.paginator.firstPage();
      }
    }
  }

}
